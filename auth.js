const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI"; //secret can be any string

//JSON Web Token or JWT is a way of securely passing information from a part of the server to the front end or to other parts of server
//Information is kept secure through the use of the secret
//Only the system that knows the secret can open the information

//Imagine JWT as a gift wrapping service but with a secret
//Only the person who knows the secret code can open the gift
//And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
//This ensures that the data is secure from the sender to the receiver

//it has 3 main parts
//creation of the token -> analogy: pack the gift, and sign with the secret
module.exports.createAccessToken = (user) => {
	//the data comes from the user parameter from the login
	//when the user logs in, a token will be created with data containing the user's information
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
		//Note you can add more information if you need
	}

	//create the token and using the jwt's sign function
	return jwt.sign(data, secret, {});
	//create a token from the data with secret and no additional parameters 
}


//verification of the token -> analogy: receive the gift and verify if the sender is legitimate and the gift was not tampered with
module.exports.verify = (req, res, next) => { //the next keyword tells the server to proceed if the verification is okay.
	let token = req.headers.authorization; //we put the JWT in the headers of the request.
	//in Postman, this is found in Authorization -> select "Bearer Token" -> Token value

	if(typeof token !== "undefined"){ //if the token is present

		console.log(token); //simply a check to see what the token's value is, can be removed later
		token = token.slice(7, token.length);
		//Recall slice function for JS - cuts the string starting from the 1st value up to the specified value
		//in this case, it starts from the 7th character to the end of the string
		//This is because the first 7 characters is not relevant to the actual token
		//ex. Bearer 7sdfkjjjkioqiwoieioklsdfjhkad
		//we don't need the word "Bearer " so we remove it using slice

		//after the token has been sliced, we then use jwt's verify function
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.send({auth : "failed"});
			} else {
				next();
			}
		})
		//jwt.verify verifies the token using the secret and fails if the token's secret does not match the secret phrase (meaning the token has been tampered with)
		//next() tells the server to allow us to proceed with the next request


	} else { //if the token is not present
		return res.send({auth : "failed"})
	}


}

//decoding of the token -> analogy : open the gift and get the content
module.exports.decode = (token) => {
	//check if the token is present of not
	if(typeof token !== "undefined"){ //there is a token present

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err){
				return null
			} else {
				return jwt.decode(token, {complete:true}).payload
				//jwt.decode -> decodes the token and get the payload
				//payload is the data from the token we create from createAccessToken
				//(the one with _id, the email, and the isAdmin)
			}
		})

	} else { //there is no token
		return null
	}
}